﻿using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.Repository.Implement;
using Amazon.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.UOW.Interface
{
    public interface IUnitOfWork
    {
        int Commit();
        DbContextTransaction BeginTransaction();
        void RollBack();
      //  ApplicationDbContext DbContext { get; }

        //IGenericRepository<AttributeHouse> AttributeHouseRepository { get; }
        //IGenericRepository<House> HouseRepository { get; }
        //IGenericRepository<HouseAttributeHouse> HouseAttributeHouseRepository { get; }
        //IGenericRepository<Image> ImageRepository { get; }
        //IGenericRepository<Post> PostRepository { get; }
        //IGenericRepository<ValueAttribute> ValueAttributeRepository { get; }
        //IGenericRepository<RentalDetail> RentalDetailRepository { get; }

        IGenericRepository<Category> CategoryRepository { get;}
        IGenericRepository<ChildCategory> ChildCategoryRepository { get; }
        IGenericRepository<Commission> CommissionRepository { get; }
        IGenericRepository<District> DistrictRepository { get; }
        IGenericRepository<Order> OrderRepository { get; }
        IGenericRepository<OrderDetail> OrderDetailRepository { get; }
        IGenericRepository<Product> ProductRepository { get; }
        IGenericRepository<ProductPhoto> ProductPhotoRepository { get; }
        IGenericRepository<ProductPromotion> ProductPromotionRepository { get; }
        IGenericRepository<ProductProperty> ProductPropertyRepository { get; }
        IGenericRepository<Province> ProvinceRepository { get; }
        IGenericRepository<Shop> ShopRepository { get; }
        IGenericRepository<Tag> TagRepository { get; }
        IGenericRepository<Ward> WardRepository { get; }

        Task SaveAsync();



    }
}
