namespace Amazon.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropProductColorandAddProductProperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductColors", "ProductID", "dbo.Products");
            DropIndex("dbo.ProductColors", new[] { "ProductID" });
            CreateTable(
                "dbo.ProductProperties",
                c => new
                    {
                        ProductPropertyID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        ProprertyXML = c.String(),
                    })
                .PrimaryKey(t => t.ProductPropertyID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID);
            
            DropTable("dbo.ProductColors");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductColors",
                c => new
                    {
                        ProductColorID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        ColorName = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.ProductColorID);
            
            DropForeignKey("dbo.ProductProperties", "ProductID", "dbo.Products");
            DropIndex("dbo.ProductProperties", new[] { "ProductID" });
            DropTable("dbo.ProductProperties");
            CreateIndex("dbo.ProductColors", "ProductID");
            AddForeignKey("dbo.ProductColors", "ProductID", "dbo.Products", "ProductID", cascadeDelete: true);
        }
    }
}
