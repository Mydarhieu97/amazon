﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Amazon.Models.DataModel
{
    public class ProductPhoto
    {
        [Key]
        public Guid PoductPhotoID { get; set; }
        [Required]
        public Guid ProductID { get; set; }
        public Product Product { get; set; }
        public string PhotoPath { get; set; }
        public int Status { get; set; }
    }
}