﻿using Amazon.Domain.Models.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Amazon.Models.DataModel
{
    public class Category
    {
        [Key]
        public Guid CategoryID { get; set; }
        [Required]
        public string CategoryName { get; set; }
        public string CategoryAvatar { get; set; }
        public virtual ICollection<ChildCategory> ChildCategories { get; set; }


       // public Guid Id { get; set; }
    }
}