﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Domain.Models.DataModel
{
   public class Notification
    {
        [Key]
        public Guid NotificationID { get; set; }
        public string NotificationContent { get; set; }
        public int Status { get; set; }
        public DateTime CreateAt { get; set; }
        public string UserUserID { get; set; }
    }
}
