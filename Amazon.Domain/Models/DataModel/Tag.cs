﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Amazon.Models.DataModel
{
    public class Tag
    {
        [Key]
        public Guid TagID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid ChildCategoryID { get; set; }
        public string TagName { get; set; }
    }
}