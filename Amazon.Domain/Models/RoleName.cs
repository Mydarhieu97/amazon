﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.Domain.Models
{
    public class RoleName
    {
        public const string Admin = "Admin";
        public const string Shop = "Shop";
        public const string Users = "Users";
    }
}
