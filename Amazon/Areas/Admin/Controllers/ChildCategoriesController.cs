﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Amazon.AppService.Interface;
using Amazon.Models;
using Amazon.Models.DataModel;

namespace Amazon.Areas.Admin.Controllers
{
    public class ChildCategoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public readonly IChildCategoryService _childCategoryservice;
        
        public ChildCategoriesController(IChildCategoryService iChildCategoryService)
        {
            _childCategoryservice = iChildCategoryService;
        }

        //get list child Category
        public ActionResult Index()
        {
            return View(_childCategoryservice.GetAllChildCategory());
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChildCategory childCategory = db.ChildCategory.Find(id);
            if (childCategory == null)
            {
                return HttpNotFound();
            }
            return View(childCategory);
        }
       
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ChildCategoryID,CategoryID,ChildCategoryName,ChildCategoryAvatar")] ChildCategory childCategory/*, HttpPostedFileBase imageChildCategory*/)
        {
            if (ModelState.IsValid)
            {
                string path="";
                //if (imageChildCategory != null && imageChildCategory.ContentLength > 0)
                //    try
                //    {
                //        path = Path.Combine(Server.MapPath("~/Content/LocalImage"),
                //                                   Path.GetFileName(imageChildCategory.FileName));
                //        imageChildCategory.SaveAs(path);
                //        ViewBag.Message = "File uploaded successfully";
                //    }
                //    catch (Exception ex)
                //    {
                //        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                //    }
                
                childCategory.ChildCategoryID = Guid.NewGuid();
                childCategory.ChildCategoryAvatar = path;
                _childCategoryservice.CreateChildCategory(childCategory);

                //db.ChildCategory.Add(childCategory);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName", childCategory.CategoryID);
            return View(childCategory);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateChildCategory([Bind(Include = "ChildCategoryID,CategoryID,ChildCategoryName,ChildCategoryAvatar")] ChildCategory childCategory/*, HttpPostedFileBase imageChildCategory*/)
        {
            int m = 0;
            return View();
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChildCategory childCategory = db.ChildCategory.Find(id);
            if (childCategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName", childCategory.CategoryID);
            return View(childCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChildCategoryID,CategoryID,ChildCategoryName,ChildCategoryAvatar")] ChildCategory childCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(childCategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Category, "CategoryID", "CategoryName", childCategory.CategoryID);
            return View(childCategory);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChildCategory childCategory = db.ChildCategory.Find(id);
            if (childCategory == null)
            {
                return HttpNotFound();
            }
            return View(childCategory);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ChildCategory childCategory = db.ChildCategory.Find(id);
            db.ChildCategory.Remove(childCategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
