﻿using System;
using System.Web;
using System.Web.Mvc;
using Amazon.Models.DataModel;
using Amazon.AppService.Interface;
using Amazon.UOW.Interface;
using Microsoft.AspNet.Identity.Owin;
using System.Net;
using Microsoft.AspNet.Identity;
using Amazon.Models;
using Amazon.Domain.Models;

namespace Amazon.Areas.Admin.Controllers
{
    public class ShopController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //private ApplicationDbContext _context = new ApplicationDbContext();
        private IUnitOfWork _unitOfWork;
        private IShopService _shopService;
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: ShopManager/ShopManager
        public ShopController(IShopService shopService, IUnitOfWork unitOfWork)
        {
            _shopService = shopService;
            _unitOfWork = unitOfWork;
        }
        [Authorize(Roles = RoleName.Admin)]
        public ActionResult Index()
        {
            var shop = _shopService.GetAll();
            return View(shop);
        }
        [Authorize(Roles = RoleName.Admin)]
        [AllowAnonymous]
        public ActionResult AddShop()
        {

            return View();
        }
        [Authorize(Roles = RoleName.Admin)]
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveShop(Shop shop)
        {
            ViewBag.WardID = new SelectList(db.Ward, "Id", "Name", shop.WardID);
            return View();
        }
        public ActionResult SaveShop(ShopRegisterViewModel shop)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = shop.Email, Email = shop.Email };
                var result = UserManager.Create(user, shop.Password);
                if (result.Succeeded)
                {

                    UserManager.AddToRoles(user.Id, "Shop");
                    _shopService.CreateShop(new Shop
                    {
                        ShopID = Guid.NewGuid(),
                        UserID = user.Id,
                        ShopName = shop.ShopName,
                        WardID = shop.WardID,
                        Status = 1

                    });
                    return RedirectToAction("Index", "Shop");
                }
            }
            ViewBag.WardID = new SelectList(db.Ward, "Id", "Name", shop.WardID);
            return View();
        }
        [Authorize(Roles = RoleName.Admin)]
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteShop(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id == null)
            {
                return HttpNotFound();
            }
            _shopService.DeleteShop(id);
            return RedirectToAction("Index","Shop");
        }
        [Authorize(Roles = RoleName.Admin)]
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ShopDetail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (id == null)
            {
                return HttpNotFound();
            }
            return View();
        }
        public ActionResult ShopDetail(Shop model)
        {
            _shopService.UpdateShop(model);
            return RedirectToAction("Index","Shop");
        }
    }
}