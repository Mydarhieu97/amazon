﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Amazon.AppService.Interface;
using Amazon.AppService.ViewModel;
using Amazon.Models;
using Amazon.Models.DataModel;

namespace Amazon.Areas.Admin.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public readonly IProductService _productService;
        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }
       
        public ActionResult Index()
        {
            //var product = db.Product.Include(p => p.ChildCategory).Include(p => p.Shop);
            //return View(product.ToList());

            return View(_productService.GetListProduct());
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        public ActionResult Create()
        {
            ViewBag.Categoryviewbag1 = new SelectList(db.Category, "CategoryID", "CategoryName");
            ViewBag.Categoryviewbag = db.Category.ToList();
          //  ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName");
            ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName");
            return View();
        }
        public JsonResult GetChildCategoryByIdCategory(Guid IdCategory)
        {
            return Json(db.ChildCategory.Where(n=>n.CategoryID== IdCategory).
                Select(s=>new { IdCate=s.ChildCategoryID,NameCate=s.ChildCategoryName }), JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProduct(FormCollection fc, HttpPostedFileBase fileImageProduct)
        {
            if (ModelState.IsValid)
            {
                ProductViewModel oneProduct = new ProductViewModel();
                oneProduct.ProductID = Guid.NewGuid();
                //shopid

                Guid ShopId = Guid.Parse(fc["ShopId"]);
                oneProduct.ShopID = ShopId;
                Guid idChicategory = Guid.Parse(fc["_idChildCategory"]);
                oneProduct.ChildCategoryID = idChicategory;
                oneProduct.Status = 1;
                string quantitynumber = fc["Quantity"];
                oneProduct.Quantity = Int32.Parse(quantitynumber);
                oneProduct.MinOrder = Int32.Parse(fc["MinOrder"]);

                oneProduct.Price = Int32.Parse(fc["Price"]);
                oneProduct.Description = fc["contentDescription"];

                //gọi service để 
                //thêm product
                //code thêm propety

                //saveFile
                string path = "";
                if (fileImageProduct != null && fileImageProduct.ContentLength > 0)
                    try
                    {
                        path = Path.Combine(Server.MapPath("~/Content/LocalImage"),
                                                   Path.GetFileName(fileImageProduct.FileName));
                        fileImageProduct.SaveAs(path);
                        ViewBag.Message = "File uploaded successfully";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                //save link path of product
                oneProduct.ProductAvatar = path;

                //gửi model sang service để lưu vào db
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,ProductName,ShopID,Quantity,ProductAvatar,Description,Status,MinOrder,Price,ChildCategoryID")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.ProductID = Guid.NewGuid();
                db.Product.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName", product.ChildCategoryID);
            ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName", product.ShopID);
            return View(product);
        }

        // GET: Admin/Products/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName", product.ChildCategoryID);
            ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName", product.ShopID);
            return View(product);
        }

        // POST: Admin/Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,ProductName,ShopID,Quantity,ProductAvatar,Description,Status,MinOrder,Price,ChildCategoryID")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChildCategoryID = new SelectList(db.ChildCategory, "ChildCategoryID", "ChildCategoryName", product.ChildCategoryID);
            ViewBag.ShopID = new SelectList(db.Shop, "ShopID", "ShopName", product.ShopID);
            return View(product);
        }

        // GET: Admin/Products/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Admin/Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Product product = db.Product.Find(id);
            db.Product.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
