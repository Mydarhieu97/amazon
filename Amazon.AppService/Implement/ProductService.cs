﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Amazon.AppService.Interface;
using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.UOW.Interface;

namespace Amazon.AppService.Implement
{
    public class ProductService : IProductService
    {
        public ApplicationDbContext DbContext = new ApplicationDbContext();
        public IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void CreateProduct(HttpPostedFileBase file, Product product)
        {
            //code here using unit of work

            //check xem shopid có tồn tại không
            //check file ảnh trùng
            //check số lượng minorder,price,quantity >0
            //status =1;
            //check id childcategory

            //create  record property of product





            throw new NotImplementedException();
        }

        public void DeleteProduct(Guid idProduct)
        {
            //code here
            throw new NotImplementedException();
        }


        public List<Product> GetListPoductByIdChildCategory(Guid idChildCategory)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetListProduct()
        {
          // var product= DbContext.Product.Include(p => p.ChildCategory).Include(p => p.Shop).ToList();
            var product = (from a in DbContext.Product
                     join b in DbContext.ChildCategory on a.ChildCategoryID equals b.ChildCategoryID
                     join c in DbContext.Shop on a.ShopID equals c.ShopID
                     select a).ToList();

            return product;
            //var product = db.Product.Include(p => p.ChildCategory).Include(p => p.Shop);
            //return View(product.ToList());
        }

        public void UpdateProduct(Product product)
        {
            throw new NotImplementedException();
        }
    }
}
