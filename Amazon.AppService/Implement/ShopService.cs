﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.AppService.Interface;
using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.UOW.Interface;
using Amazon.Domain.Models;

namespace Amazon.AppService.Implement
{
    public class ShopService : IShopService
    {
        ApplicationDbContext db = new ApplicationDbContext();

        private readonly IUnitOfWork _unitOfWork;

        public ShopService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void CreateShop(Shop shop)
        {
            _unitOfWork.ShopRepository.Create(shop);
        }

        public void DeleteShop(Guid idShop)
        {
            Shop shop = db.Shop.Find(idShop);
            db.Shop.Remove(shop);
            db.SaveChanges();
        }

        public Shop GetShopByIdUser(string idUser)
        {
            throw new NotImplementedException();
        }
        public IQueryable<Shop> GetAll()
        {
            var shop = _unitOfWork.ShopRepository.GetAll().Include(m => m.Ward);
            //shop = db.Shop.Include(m => m.Ward);
            return shop;

        }
        public void UpdateShop(Shop shop)
        {
            _unitOfWork.ShopRepository.Update(shop);
            throw new NotImplementedException();
        }
    }
}
