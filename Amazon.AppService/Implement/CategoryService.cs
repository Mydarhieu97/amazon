﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.AppService.Interface;
using Amazon.Models;
using Amazon.Models.DataModel;
using Amazon.UOW.Implement;
using Amazon.UOW.Interface;
namespace Amazon.AppService.Implement
{
    public class CategoryService : ICategoryService
    {
       public ApplicationDbContext DbContext = new ApplicationDbContext();

        public IUnitOfWork _unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public void CreateCategory(Category category)
        {
            _unitOfWork.CategoryRepository.Create(category);
        }

        public void DeleteCategory(Guid idCategory)
        {
            throw new NotImplementedException();
        }

        public List<Category> GetAllCategory()
        {
           // List<Category> listCate = (from a in DbContext.Category select a).ToList();
            var listCategory= _unitOfWork.CategoryRepository.GetAll().ToList();
            return listCategory;
        }

        public void UpdateCategory(Category category)
        {
            _unitOfWork.CategoryRepository.Update(category);
        }
    }
}
