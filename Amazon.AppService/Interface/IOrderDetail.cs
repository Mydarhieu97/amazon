﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
   public interface IOrderDetail
    {
        //get one order by id- user/id order.....
        OrderDetail GetOrderDetailById(Guid id);
        List<OrderDetail> GetListOrderByIdOrder(Guid idOrder);

        void CreateOrderDetail(OrderDetail orderDetail);
        void UpdateOrderDetail(OrderDetail orderDetail);
        void DeleteOrderDetail(Guid idOrderDetail);
    }
}
