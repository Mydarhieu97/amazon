﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
   public interface IChildCategoryService
    {
        void CreateChildCategory( ChildCategory childCategory);
        void UpdateChildCategory(ChildCategory childCategory);

        List<ChildCategory> GetAllChildCategory();
        ChildCategory GetChildCategoryByIdCategory(Guid idCategory);


        //not use this function delete
        void DeleteChildCategory(Guid idCategory);
    }
}
