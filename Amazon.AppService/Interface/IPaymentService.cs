﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
  public  interface IPaymentService
    {
        void CreatePayment(Payment payment);
        void UpdatePayment(Payment payment);
        void DeletePayment(Guid IdPayment);
    }
}
