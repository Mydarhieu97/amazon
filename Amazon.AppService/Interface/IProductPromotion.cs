﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;
namespace Amazon.AppService.Interface
{
   public interface IProductPromotion
    {
        void CreateProductPromotion(ProductPromotion productPromotion);
        void UpdateProductPromotion(ProductPromotion productPromotion);
        void DeleteProductPromotion(Guid ProductPromotion);
        ProductPromotion GetPromotionByIdProduct(Guid idProduct);
    }
}
