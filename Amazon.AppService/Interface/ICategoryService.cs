﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Models.DataModel;

namespace Amazon.AppService.Interface
{
   public interface ICategoryService
    {
        List<Category> GetAllCategory();
        void CreateCategory(Category category);
        void DeleteCategory(Guid idCategory);
        void UpdateCategory(Category category);

    }
}
