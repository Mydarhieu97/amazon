﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Amazon.Models.DataModel;
using Amazon.AppService.ViewModel;
namespace Amazon.AppService.Mapping
{
  public  class ModelMapping:Profile
    {
        public  ModelMapping()
        {
            CreateMap<Category, CategoryViewModel>();
            CreateMap<ChildCategory, ChildCategoryViewModel>();
            CreateMap<Commission, CommissionViewModel>();
            CreateMap<OrderDetail, OrderDetailViewModel>();
            CreateMap<Payment, PaymentViewModel>();
            CreateMap<ProductPhoto, ProductPhotoViewModel>();
            CreateMap<ProductPromotion, ProductPromotionViewModel>();
            CreateMap<ProductProperty, ProductPropertyViewModel>();
            CreateMap<Shop, ShopViewModel>();
            CreateMap<Tag, TagViewModel>();


  //          cfg.CreateMap<Order, OrderDto>()
              //.ForMember(d => d.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
              //.ReverseMap()
              //.ForPath(s => s.Customer.Name, opt => opt.MapFrom(src => src.CustomerName));
        }
        //public static void Re
    }
}
