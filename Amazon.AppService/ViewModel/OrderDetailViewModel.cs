﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class OrderDetailViewModel
    {
        public Guid OrderDetailID { get; set; }
        public Guid OrderID { get; set; }
        public Guid ProductID { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }
        public int Status { get; set; }
        public DateTime TimeUpdate { get; set; }

        public string ProductName { get; set; }
    }
}
