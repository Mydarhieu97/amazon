﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class CategoryViewModel
    {
        public Guid CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryAvatar { get; set; }
    }
}
