﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
  public   class CommissionViewModel
    {
        public Guid CommissionID { get; set; }
        public Guid ShopID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime PayDate { get; set; }
        public float TotalMoney { get; set; }
        public int Status { get; set; }

        public string ShopName { get; set; }
    }
}
