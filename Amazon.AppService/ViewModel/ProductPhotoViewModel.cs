﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazon.AppService.ViewModel
{
   public class ProductPhotoViewModel
    {
        public Guid PoductPhotoID { get; set; }
        public Guid ProductID { get; set; }
        public string PhotoPath { get; set; }
        public int Status { get; set; }

        public string ProductName { get; set; }
       
    }
}
